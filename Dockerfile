FROM centos

MAINTAINER apache_tpesag_docker

RUN mkdir /opt/TOTVSAgro/

WORKDIR /opt/TOTVSAgro
COPY apache.tar.gz /opt/TOTVSAgro/apache.tar.gz
RUN tar xvfz apache.tar.gz

COPY jdk-8u251-linux-i586.tar.gz /opt/TOTVSAgro/jdk-8u251-linux-i586.tar.gz
RUN tar xvfz jdk-8u251-linux-i586.tar.gz

WORKDIR /opt/TOTVSAgro/apache-tomcat-9.0.10/webapps
#RUN curl -O -L https://github.com/AKSarav/SampleWebApp/raw/master/dist/SampleWebApp.war

EXPOSE 8080 8180

#CMD ["/opt/TOTVSAgro/apache-tomcat-9.0.10/bin/catalina.sh", "run"]